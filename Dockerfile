FROM openjdk:8-jdk-alpine
MAINTAINER 'jiwan8985' 'jiwan8985@sicc.co.kr'
VOLUME /tech
ENV TZ=Asia/Seoul
ENV LANG=ko_KR.UTF-8
ENV LANGUAGE=ko_KR.UTF-8
ENV LC_ALL=ko.KR.UTF-8
ENV JAVA_OPTS="-XX:PermSize=1024m -XX:MaxPermSize=512m -Xmx4g -Xms2g"
COPY ./app.jar app.jar
ENTRYPOINT ["java", "-jar","/app.jar"]