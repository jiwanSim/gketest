package com.gke.sicc.devops.jiwan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GkeTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(GkeTestApplication.class, args);
	}

}
